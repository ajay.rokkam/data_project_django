"""django_d-ata_project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django_data_project import views

urlpatterns = [
    path('', views.index, name='index'),
    path('data_project_1/', views.matches_per_season, name='matches_per_season'),
    path('data_project_2/', views.matches_by_team, name='matches_by_team'),
    path('data_project_3/', views.extras_by_team, name='extras_by_team'),
    path('data_project_4/', views.economical_bowlers, name='economical_bowlers'),
    path('data_project_5/', views.sixes_per_season, name='sixes_per_season'),
    
]
