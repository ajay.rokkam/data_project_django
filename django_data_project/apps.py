from django.apps import AppConfig


class DjangoDataProjectConfig(AppConfig):
    name = 'django_data_project'
