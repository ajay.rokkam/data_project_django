from django.core.management.base import BaseCommand, CommandError
from django_data_project.models import Matches, Deliveries
import csv, pymysql


class Command(BaseCommand):
    help = "Loads csv file to database"



    def handle(self, *args, **options):
        mydb = pymysql.connect(
            host = 'localhost',
            user = 'dev',
            passwd='dev',
            database='data_project_db',
            autocommit=True,
            local_infile=1
        )

        mycursor = mydb.cursor()

        query_to_transfer_matches_to_mysql = "LOAD DATA LOCAL INFILE '/var/lib/mysql-files/matches.csv' INTO TABLE data_project_db.django_data_project_matches FIELDS TERMINATED BY ','  IGNORE 1 LINES;"
        mycursor.execute(query_to_transfer_matches_to_mysql)

        query_to_transfer_deliveries_to_mysql = "LOAD DATA LOCAL INFILE '/var/lib/mysql-files/deliveries.csv' INTO TABLE data_project_db.django_data_project_deliveries FIELDS TERMINATED BY ','  IGNORE 1 LINES(match_id,inning,batting_team,bowling_team,over,ball,batsman,non_striker,bowler,is_super_over,wide_runs,bye_runs, legbye_runs, noball_runs, penalty_runs, batsman_runs, extra_runs, total_runs, player_dismissed, dismissal_kind, fielder);"
        mycursor.execute(query_to_transfer_deliveries_to_mysql)

        mycursor.close()
        mydb.close()
            
