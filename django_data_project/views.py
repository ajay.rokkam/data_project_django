from django.shortcuts import render
from .models import Matches, Deliveries
from django.db.models import Count, Avg, Sum, Subquery
from django.conf import settings
from django.core.cache.backends.base import DEFAULT_TIMEOUT
from django.views.decorators.cache import cache_page

CACHE_TTL = getattr(settings, 'CACHE_TTL', DEFAULT_TIMEOUT)


# Create your views here

def index(request):
    return render(request, 'data_project/index.html')

@cache_page(CACHE_TTL)
def matches_per_season(request):
    matches_per_season = Matches.objects.values('season').annotate(Count('season')).order_by('season')
    return render(request, 'data_project/matches_per_season.html', {'matches_per_season': matches_per_season,
    'title': 'Matches per year',
    'xaxis': 'seasons'})

@cache_page(CACHE_TTL)
def matches_by_team(request):
    matches_by_team = Matches.objects.values('winner', 'season').annotate(Count('winner'))
    matches_won_by_all_teams = {}
    seasons = {'2008': 0, '2009': 0, '2010': 0, '2011': 0, '2012': 0, '2013': 0, '2014': 0, '2015': 0, '2016': 0, '2017': 0}
    for team in matches_by_team:
        if team['winner'] is not '' and team['winner'] not in matches_won_by_all_teams.keys():
            matches_won_by_all_teams[team['winner']] = {'2008': 0, '2009': 0, '2010': 0, '2011': 0, '2012': 0, '2013': 0, '2014': 0, '2015': 0, '2016': 0, '2017': 0}
            matches_won_by_all_teams[team['winner']][str(team['season'])] = team['winner__count']
        elif team['winner'] is not '':
            matches_won_by_all_teams[team['winner']][str(team['season'])] = team['winner__count']
    return render(request, 'data_project/matches_by_team.html', {'categories': [season for season in sorted(seasons)],
    'matches_per_season': matches_won_by_all_teams,
    'title': 'Matches per year',
    'xaxis': 'seasons'})

@cache_page(CACHE_TTL)
def extras_by_team(request):
    matches_set = Matches.objects.only('season','id').filter(season=2016)
    extras_by_team = Deliveries.objects.only('match_id','bowling_team', 'extra_runs').filter(match_id__in=matches_set).only('bowling_team').values("bowling_team").annotate(runs=Sum("extra_runs"))
    return render(request, 'data_project/extras_per_team.html', {'extras_by_team': extras_by_team,
    'title': 'Extras per team in 2016',
    'xaxis': 'Teams'})

@cache_page(CACHE_TTL)
def economical_bowlers(request):
    match2015 = Matches.objects.only("season", "id").filter(season=2015)
    bowlers = Deliveries.objects.filter(match_id__in=match2015).values("bowler").annotate(avg=Avg("total_runs")*6).order_by('avg')[:8]
    return render(request, 'data_project/economical_bowlers.html', {'bowlers': bowlers,
    'title': 'Top economical bowlers',
    'xaxis': 'bowlers'})

@cache_page(CACHE_TTL)
def sixes_per_season(request):
    sixes_by_season = Deliveries.objects.only('match_id').filter(batsman_runs=6).select_related('id').values('match__season').annotate(Count('match__season')).order_by('match__season')
    return render(request, 'data_project/sixes_per_season.html', {'sixes_by_season': sixes_by_season,
    'title': 'Sixes per Season',
    'xaxis': 'seasons'})